## Installation

- Clone the repository

## Build

- `source ./build-styles.sh`

## Start

- Open `index.html` in browser (better in latest stable Google Chrome)

## Notes

- This site uses SASS (`.scss`), so before we run this site, we need to build styles (see **Build**)
- This site is designed for PCs, laptops, tablets and mobile phones. No need to make extremely big or small screen size :)
